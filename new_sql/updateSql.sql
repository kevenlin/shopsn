ALTER TABLE `db_user` MODIFY COLUMN `level_id` SMALLINT(3) NOT NULL COMMENT '等级编号'/*添加level_id 的注释*/

DROP TABLE IF EXISTS `db_user`;

CREATE TABLE `db_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户编号',
  `mobile` varchar(11) DEFAULT NULL COMMENT '电话号码  空为未开启手机验证',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  `status` tinyint(1) DEFAULT '1' COMMENT '账号状态   1正常   0禁用',
  `update_time` int(11) DEFAULT NULL COMMENT '更新时间',
  `open_id` varchar(255) DEFAULT NULL COMMENT 'openid是公众号的普通用户的一个唯一的标识',
  `password` varchar(40) DEFAULT NULL COMMENT '密码',
  `user_name` varchar(64) DEFAULT NULL COMMENT '用户名',
  `nick_name` varchar(64) DEFAULT NULL COMMENT '昵称',
  `birthday` date DEFAULT NULL COMMENT '生日',
  `id_card` varchar(64) DEFAULT NULL COMMENT '身份证号码',
  `email` varchar(64) DEFAULT NULL COMMENT '邮箱',
  `level_id` smallint(3) NOT NULL DEFAULT '1',
  `sex` varchar(4) DEFAULT NULL COMMENT '性别',
  `integral` int(11) NOT NULL DEFAULT '0' COMMENT '积分',
  `last_logon_time` int(11) NOT NULL DEFAULT '0' COMMENT '上次登录时间',
  `salt` varchar(255) DEFAULT NULL COMMENT '加盐字段： 和密码进行加密，增加密码强度',
  `recommendcode` varchar(100) NOT NULL DEFAULT '0' COMMENT '推荐人编码',
  `validate_email` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否验证邮箱',
  `member_status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0普通会员 1 渠道会员，2 月结会员',
  `member_discount` decimal(5,2) NOT NULL DEFAULT '100.00' COMMENT '折扣率',
  `p_id` int(11) NOT NULL DEFAULT '0' COMMENT '父级会员编号',
  PRIMARY KEY (`id`),
  UNIQUE KEY `mobile` (`mobile`) USING BTREE COMMENT '手机号设置为唯一标示',
  UNIQUE KEY `name` (`user_name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=276 DEFAULT CHARSET=utf8 COMMENT='会员表';

/*Data for the table `db_user` */

insert  into `db_user`(`id`,`mobile`,`create_time`,`status`,`update_time`,`open_id`,`password`,`user_name`,`nick_name`,`birthday`,`id_card`,`email`,`level_id`,`sex`,`integral`,`last_logon_time`,`salt`,`recommendcode`,`validate_email`,`member_status`,`member_discount`,`p_id`) values (3,'13052079525',1484968184,1,1484968184,'0000-00-00 00:00:00','d35a990cb5f96bf1922c7bf9048e8aa5','hxh',NULL,NULL,NULL,'2272597637@qq.com',3,NULL,12345,1503370999,NULL,'',0,0,'90.00',0),(273,'17681139991',1502846723,1,NULL,NULL,'e10adc3949ba59abbe56e057f20f883e','青菜',NULL,NULL,NULL,'123123@qq.com',3,NULL,0,1505784834,NULL,'0',0,0,'90.00',0);

/*Table structure for table `db_wx_user` */

DROP TABLE IF EXISTS `db_wx_user`;

CREATE TABLE `db_wx_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '表id',
  `uid` int(11) NOT NULL COMMENT 'uid',
  `wxname` varchar(60) NOT NULL DEFAULT '' COMMENT '公众号名称',
  `aeskey` varchar(256) NOT NULL DEFAULT '' COMMENT 'aeskey',
  `encode` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'encode',
  `appid` varchar(50) NOT NULL DEFAULT '' COMMENT 'appid',
  `appsecret` varchar(50) NOT NULL DEFAULT '' COMMENT 'appsecret',
  `wxid` varchar(64) DEFAULT '' COMMENT '公众号原始ID',
  `weixin` char(64) DEFAULT NULL COMMENT '微信号',
  `headerpic` char(255) NOT NULL COMMENT '头像地址',
  `token` char(255) DEFAULT NULL COMMENT 'token',
  `w_token` varchar(150) DEFAULT '' COMMENT '微信对接token',
  `create_time` int(11) DEFAULT NULL COMMENT 'create_time',
  `updatetime` int(11) DEFAULT NULL COMMENT 'updatetime',
  `tplcontentid` varchar(2) DEFAULT '' COMMENT '内容模版ID',
  `share_ticket` varchar(150) DEFAULT '' COMMENT '分享ticket',
  `share_dated` char(15) DEFAULT NULL COMMENT 'share_dated',
  `authorizer_access_token` varchar(200) DEFAULT '' COMMENT 'authorizer_access_token',
  `authorizer_refresh_token` varchar(200) DEFAULT '' COMMENT 'authorizer_refresh_token',
  `authorizer_expires` char(10) DEFAULT NULL COMMENT 'authorizer_expires',
  `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '类型',
  `web_access_token` varchar(200) NOT NULL DEFAULT '' COMMENT ' 网页授权token',
  `web_refresh_token` varchar(200) NOT NULL DEFAULT '' COMMENT 'web_refresh_token',
  `web_expires` int(11) NOT NULL COMMENT '过期时间',
  `qr` varchar(200) DEFAULT '' COMMENT 'qr',
  `menu_config` text COMMENT '菜单',
  `wait_access` tinyint(1) DEFAULT '0' COMMENT '微信接入状态,0待接入1已接入',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`) USING BTREE,
  KEY `uid_2` (`uid`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COMMENT='微信公共帐号';

/*Data for the table `db_wx_user` */

insert  into `db_wx_user`(`id`,`uid`,`wxname`,`aeskey`,`encode`,`appid`,`appsecret`,`wxid`,`weixin`,`headerpic`,`token`,`w_token`,`create_time`,`updatetime`,`tplcontentid`,`share_ticket`,`share_dated`,`authorizer_access_token`,`authorizer_refresh_token`,`authorizer_expires`,`type`,`web_access_token`,`web_refresh_token`,`web_expires`,`qr`,`menu_config`,`wait_access`) values (1,0,'ThinkphpShop','',0,'8a4963a14f3b1','62cb0a3111e65346b849ac88958','1234567890','1234567890@163.com','/public/upload/weixin/2017/05-22/3955fbbfd052fb1b34e1f8d931bb8d7c.jpg','eesops1462769263','qingcai111222',1486797230,0,'','','','','','',1,'','',0,'/public/upload/weixin/2017/05-22/f336477a647a83388feb9a82b5a2a889.jpg',NULL,1);
DROP TABLE IF EXISTS `db_wx_menu`;

CREATE TABLE `db_wx_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `level` tinyint(1) DEFAULT '1' COMMENT '菜单级别',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT 'name',
  `sort` int(5) DEFAULT '0' COMMENT '排序',
  `type` varchar(20) DEFAULT '' COMMENT '0 view 1 click',
  `value` varchar(255) DEFAULT NULL COMMENT 'value',
  `token` varchar(50) NOT NULL DEFAULT '' COMMENT 'token',
  `pid` int(11) DEFAULT '0' COMMENT '上级菜单',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=86 DEFAULT CHARSET=utf8;

/*Data for the table `db_wx_menu` */

insert  into `db_wx_menu`(`id`,`level`,`name`,`sort`,`type`,`value`,`token`,`pid`) values (28,1,'系统设置',0,'view','','eesops1462769263',0);
insert  into `db_wx_menu`(`id`,`level`,`name`,`sort`,`type`,`value`,`token`,`pid`) values (85,1,'商城',0,'view','http://m.shopsn.net/','',0);
insert  into `db_wx_menu`(`id`,`level`,`name`,`sort`,`type`,`value`,`token`,`pid`) values (33,1,'会员',0,'view','','eesops1462769263',0);
insert  into `db_wx_menu`(`id`,`level`,`name`,`sort`,`type`,`value`,`token`,`pid`) values (82,1,'会员列表',0,'view','http://www.tp-shop.cn/index.php/Mobile/','',33);
insert  into `db_wx_menu`(`id`,`level`,`name`,`sort`,`type`,`value`,`token`,`pid`) values (80,1,'系统链接',0,'view','http://www.tp-shop.cn/index.php/Mobile/','',28);


